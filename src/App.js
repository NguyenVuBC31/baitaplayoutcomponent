import logo from "./logo.svg";
import "./App.css";
import "./Styles.css";
import Header from "./BaiTapThucHanhLayout/Header";
import Footer from "./BaiTapThucHanhLayout/Footer";
import Body from "./BaiTapThucHanhLayout/Body";
import Item from "./BaiTapThucHanhLayout/Item";

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Item />
      <Footer />
    </div>
  );
}

export default App;
